// This is the main Node.js source code file of your actor.
// It is referenced from the "scripts" section of the package.json file,
// so that it can be started by running "npm start".

// Import Apify SDK. For more information, see https://sdk.apify.com/
const Apify = require('apify')
const pageUtils = require('./pageUtils')

Apify.main(async () => {
  // Get input of the actor (here only for demonstration purposes).
  // If you'd like to have your input checked and have Apify display
  // a user interface for it, add INPUT_SCHEMA.json file to your actor.
  // For more information, see https://apify.com/docs/actor/input-schema
  const input = await Apify.getInput()
  console.log('Input:')
  console.dir(input)

  if (!input || !input.url)
    throw new Error('Input must be a JSON object with the "url" field!')

  console.log('Launching Puppeteer...')
  const browser = await Apify.launchPuppeteer()

  console.log(`Opening page ${input.url}...`)
  const page = await browser.newPage()
  await page.goto(input.url)
  const title = await page.title()
  console.log(`Title of the page "${input.url}" is "${title}".`)

  // Login to qbo
  await page.waitForSelector('.logincontainer')
  await page.waitFor('#ius-sign-in-submit-btn')
  const qboAuth = await pageUtils.loginQbo(page)
  console.log(`Was able to authenticate qbo: ${qboAuth}`)

  // await page.waitFor('#company-list1')
  await page.$$eval('a.company_link', async anchors => {
      // todo: change to The Future of Bookkeeping company in production
      const bkAnchor = anchors.find(anchor => anchor.innerText.includes('botkeeper'))
      await bkAnchor.click()
  })


  // Option1: scraping UI for failed accounts
  // todo: find any disconnected accounts
  // todo: will we have to paginate?
  await page.waitForNavigation()
  await page.waitFor('.react-grid-Grid')

  // todo: wait for all table rows to load?
  const clientTable = await page.$$eval('.react-grid-Grid', els => els.map(el => el.textContent))
  const tableRows = await page.$$eval('.react-grid-Row', rows => rows.map(row => row.textContent))
  console.table(tableRows)

  const clientsWithFailedConnections = tableRows.filter(row => row.includes('failed'))
  console.log('*** clientsWithFailedConnections', clientsWithFailedConnections)

  // Option 2: Calling getInitialData internal api


  console.log('Saving output...')
  await Apify.setValue('OUTPUT', {
    title,
    qboAuth
    // companyList
    // lastUrlVisited: url 
  })

  // console.log('Closing Puppeteer...')
  // await browser.close()

  console.log('Done.')
})
