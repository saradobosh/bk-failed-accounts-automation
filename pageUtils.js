module.exports = {
  loginQbo: async page => {
    try {
      await page.type('#ius-userid', '')
      await page.type('#ius-password', '')
      await page.click('#ius-sign-in-submit-btn')
      const url = await page.url()

      console.log('This may take a bit if you hit 2fa...complete 2fa and wait up to 30 sec for script to continue')

      await page.waitForNavigation()

      if (url.includes('companysel')) return true

      // todo: not consistently working when determining if mfa is required
      if (url.includes('login?webredir')) {
        // https://c42.qbo.intuit.com/qbo42/login?webredir
        // select first option (text) to send msg to number associated with mfa
        await page.click('#ius-mfa-options-actions')
        // click continue button
        await page.click('#ius-mfa-options-submit-btn')
        // wait 20 seconds for user to complete 2fa
        await page.waitFor(30000)
        await page.waitFor('#company-list1')
        return true
      }
      return false
    } catch (err) {
      console.log('An error occurred logging in', err)
      return false
    }
  },
  getDomainSubstringFromUrl: async url => {
    return url.split('//').pop().split('.')[0]
  },
  getContentsOf: (page, selector) => {
    return page.$eval(selector, el => el.innerHTML)
  }
}
